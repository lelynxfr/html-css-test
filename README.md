# LeLynx onboarding Front-End  #

Merci d'avoir répondu à notre annonce. Nous vous proposons maintenant un petit exercice pour vous mettre en appétit avant que l'on se rencontre ;) Il s'agit d'intégrer une page en HTML. Rien de bien complexe, pas d'effet wahoo attendus, juste des choses simples et robustes !

Un document Photoshop est disponible à cette adresse : https://www.lelynx.fr/get-onboarding.php

### Voici quelques indices pour vous guider vers ce que nous attendons ###

* Organisez votre code habilement pour faciliter la lisibilité
* Proposez votre workflow pour un travail efficace et flexible
* Le rendu de votre page doit s'adapter à tous type de terminal ou navigateur courant
* Vous pouvez publier votre travail ici même dans une branche spécifique

Un petit plus : on attend un message de votre part au bout de ce hook :
https://hooks.slack.com/services/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

🐯